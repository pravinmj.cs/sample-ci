"""
    This module gets an username and a number between 1 and 10.
    Returns a message based on the chosen number.
"""

import unittest


class TestUser(unittest.TestCase):
    """
    Runs unit test to validate the functionalities in code.
    """

    def test_value_types(self):
        """Checks if all user objects has appropriate types.
        username has type string and lucky_no has type int"""
        sample_input_no = 32
        sample_input_name = "Ram"
        self.assertIsInstance(sample_input_no, int)
        self.assertIsInstance(sample_input_name, str)

    def test_no_range_limit(self):
        """Checks if all user objects has appropriate types.
        username has type string and lucky_no has type int"""
        sample_input_no = 5
        get_status("Ram", sample_input_no)
        self.assertGreaterEqual(sample_input_no, 1)
        self.assertLessEqual(sample_input_no, 5)


def get_status(username, lucky_no):
    """
    Gets a lucky no and return a status

    Args:
        username(str): A string value
        lucky_no(int): An integer value

    Returns:
    str: A status message based on chosen lucky_no
    """
    if int(lucky_no) == 5:
        message = f"Congratulations {username}. You have won 1000000000"
    else:
        message = "Better luck next time"
    return message


if __name__ == "__main__":
    input_name = input("Enter your name: ")
    input_no = input("Enter a number between 1-10: ")

    status = get_status(input_name, input_no)
    print(status)
